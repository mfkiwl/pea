
import operator
from app import app
from data.get_data_sol import get_sol_data
from layouts.different_graphs import epoch_graphs_layout , sites_graph_layout
from dash.dependencies import Input, Output

import dash_html_components as html
import plotly.express as px
import plotly.graph_objects as go
import pdb
from app import sol
from data.get_data_sol import get_sol_data
import numpy as np
from callbacks.callback_page2 import *
import pandas as pd
from callbacks.helperfunc import *


"""
@ app.callback()
    ************************************************************************************************************************************  
    * @brief CALLBACK :pathname == "/page-0".                                                                                          *                                                                              
    *    This callback defines the input and return params for the function to be executed when this callback is invoked.              *                                                 *
    * @param Output (Equivalent to Return statement):                                                                                  *
    *        epoch_graph: This ID, which represents a Graph created using the Dash Core Components, will be updated.                   *
    *        figure: A required placeholder, not used though                                                                           *
    * @param Input:                                                                                                                    *   
    *        summary_p_dropdown_1: Reference to the 1st DropDown Menu at the summary pg.                                               *
    *        value: Value contained by that DropDown Menu.                                                                             *   
    *                                                                                                                                  *   
    *        summary_pg_dropdown_2: Reference to the 2nd DropDown Menu at the summary pg.                                              *
    *        value: Value contained by that DropDown Menu.                                                                             *
    *  
*   ************************************************************************************************************************************   
@ update_epoch_pg_graph()
    ************************************************************************************************************
    * @brief Updates the graph on the page(pathname == "/page-2"), based on the values of DropDown attributes. *
    * @ rec: Alias for the the `value` in  reciever_pg_dropdown_1                                              *
    * @ attribute: Alias for the `value` in reciever_pg_dropdown_2     
    * @return Graph: Updated graphs based on above params.                                                     *                                                                                                                          *
    ************************************************************************************************************
"""
@app.callback(
    Output('epoch_graph', 'figure'),
    [Input('summary_pg_dropdown_1', 'value'),
     Input('summary_pg_dropdown_2', 'value')])

def update_epoch_pg_graph(epochField_1, epochField_2):

        if(epochField_1 is None and epochField_2 is None):
                return get_empty_graph("Select any value from at least one Dropdown Menu") 

        ##Create Empty trace:
        traceList = []


        epoch = pd.DatetimeIndex(sol.meta["ALL_EPOCH_VALUES"]["EPOCH"])   
      
        if(epochField_1 is not None):
                name = ""
                if(epochField_2 is not None):
                        name  = get_units(epochField_1)
                else : 
                        name = str(epochField_1)


   
                sorted_df = sort_df(epoch,sol.meta["ALL_EPOCH_VALUES"][epochField_1])
                
                trace1 = go.Scatter(x= sorted_df['x'], y = sorted_df['y'],
                mode="lines+markers",
                name=name)
                traceList.append(trace1)
         
        

        if(epochField_2 is not None):
                name = ""
                if(epochField_1 is not None):
                        name  = get_units(epochField_2)
                else : 
                        name = str(epochField_2)
        
                sorted_df = sort_df(epoch,sol.meta["ALL_EPOCH_VALUES"][epochField_2])
                
                trace2=go.Scatter(x= sorted_df['x'], y = sorted_df['y'],
                mode='lines+markers',
                name=name)
                traceList.append(trace2)


        data = traceList
        if(epochField_1 is not None):
                return get_figure(data,get_units("EPOCHS"),get_units(epochField_1) )

        if(epochField_2 is not None):
                return get_figure(data,get_units("EPOCHS"),get_units(epochField_2) )

        



        



