import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc

from app import sol,res
import pdb
 

"""
    Create fields for different sites in the  dropdown appearing at different url.
    The different URLs are : /page-1 (EPOCH@FIELDS)
                             /page-2 (SITE@FIELDS)
                             /page-3 (SAT@FIELDS)


"""

#*****************************DROPDOWN  for /page-1*************************************************************************#:
#Todo: Make it dynamic, just like fields_set_2
fields_set_1 = ["EPOCH_MEAN_PHASE_RESID","EPOCH_PHASE_RESID_RMS","EPOCH_MEAN_RANGE_RESID","EPOCH_RANGE_RESID_RMS",
         "EPOCH_POSTFIT_CHISQR_DOF","EPOCH_PREFIT_CHISQR_INC","EPOCH_NUM_SITES","EPOCH_NUM_SVS","EPOCH_NUM_OBS",
         "EPOCH_NUM_PARAMS","EPOCH_NUM_AMB","EPOCH_NUM_WL_AMB_FIXED","EPOCH_NUM_NL_AMB_FIXED"]  

summary_pg_dropdown_1=  html.Div([
            dcc.Dropdown(
                id='summary_pg_dropdown_1',
                options=[{'label': i, 'value': i} for i in fields_set_1],
                value='EPOCH_MEAN_PHASE_RESID'
            ),
    
        ],
        style={'width': '33%', 'display': 'inline-block'})

summary_pg_dropdown_2 = html.Div([
            dcc.Dropdown(
                id='summary_pg_dropdown_2',
                options=[{'label': i, 'value': i} for i in fields_set_1],
                value='EPOCH_PHASE_RESID_RMS'
            ),
        
        ],style={'width': '33%', 'float': 'right', 'display': 'inline-block'})

#*****************************DROPDOWN  for /page-1 ENDS********************************************************************#:
#************************************************************************************************************************#:



#*****************************DROPDOWN  for /page-2*************************************************************************#:
##Create REC@FIELDS
fields_set_3 = []
fields_set_3a = []


fields_set_3.append("ALL")
for key1 in sol.rec:
    fields_set_3.append(str(key1))



fields_set_3a = ["SITE_MEAN_PHASE_RESID","SITE_MEAN_RANGE_RESID","SITE_PHASE_RESID_RMS","SITE_RANGE_RESID_RMS","SITE_DX","SITE_DY", "SITE_DZ", 
"SITE_DN", "SITE_DE", "SITE_DU","SITE_X", "SITE_Y", "SITE_Z","TROP_ZWD", "TROP_GRAD_NS", "TROP_GRAD_EW","REC_CLK_EST"]

reciever_pg_dropdown_1=  html.Div([
            dcc.Dropdown(
                id='reciever_pg_dropdown_1',
                options=[{'label': i, 'value': i} for i in fields_set_3],
                # value='AGGO'
                placeholder = "Reciever"
                
            )
      
        ],
        style={'width': '33%', 'display': 'inline-block'})

reciever_pg_dropdown_2=  html.Div([
            dcc.Dropdown(
                id='reciever_pg_dropdown_2',
                options=[{'label': i, 'value': i} for i in fields_set_3a],
                # value='SITE_X'
                placeholder = "Attribute"
                
            )
      
        ],
        style={'width': '33%', 'display': 'inline-block'})



#*****************************DROPDOWN  for /page-2 ENDS********************************************************************#:
#***************************************************************************************************************************#:



#*****************************DROPDOWN  for /page-3*************************************************************************#:
##Create SAT@FIELDS
fields_set_4 = []
# fields_set_4a = []

fields_set_4.append("ALL")

for key1 in sol.sat:
     fields_set_4.append(str(key1))

# for key2 in sol.sat[key1]: 
#     fields_set_4a.append(str(key2))
fields_set_4a=["SV_MEAN_PHASE_RESID","SV_MEAN_RANGE_RESID","SV_PHASE_RESID_RMS","SV_RANGE_RESID_RMS","SV_CLK_EST","SV_X", "SV_Y", "SV_Z", "SV_VX", "SV_VY",
 "SV_VZ", "SV_DRAD","SV_YRAD","SV_BRAD" ,"SV_NRAD"]

satellite_pg_dropdown_1=  html.Div([
            dcc.Dropdown(
                id='satellite_pg_dropdown_1',
                options=[{'label': i, 'value': i} for i in fields_set_4],
                value='G01_SVN#'
                
            )
      
        ],
        style={'width': '33%', 'display': 'inline-block'})

satellite_pg_dropdown_2=  html.Div([
            dcc.Dropdown(
                id='satellite_pg_dropdown_2',
                options=[{'label': i, 'value': i} for i in fields_set_4a],
                value='SV_X'
                
            )
      
        ],
        style={'width': '33%', 'display': 'inline-block'})
#*****************************DROPDOWN  for /page-3 ENDS********************************************************************#:
#***************************************************************************************************************************#:


#*****************************DROPDOWN  for /page-4*************************************************************************#:
##Create SAT@FIELDS
fields_set_5 = []
fields_set_6 = []
fields_set_8 = []

## Todo Make the residuals_pg_dropdown_3 drop down dynamic: 


# pdb.set_trace()

for key1 in sol.rec:
    fields_set_5.append(key1)

for key1 in sol.sat:
    fields_set_6.append(key1)

# fields_set_7 = ["EPOCHS","ELEV_ANG","NADIR_ANG"]    

fields_set_8 = ["FLOAT_AMB","OBS_PHASE_RESID","OBS_RANGE_RESID"]

fields_set_5_flat =["LINE","SCATTER","POLAR","HISTOGRAM"]

   
residuals_pg_dropdown_1=  html.Div([
            dcc.Dropdown(
                id='residuals_pg_dropdown_1',
                options=[{'label': i, 'value': i} for i in fields_set_5_flat],
                placeholder = "SELECT_GRAPH_TYPE",
                value = ""
                
            )
      
        ],
        style={'width': '20%', 'display': 'inline-block'})


residuals_pg_dropdown_2=  html.Div([
            dcc.Dropdown(
                id='residuals_pg_dropdown_2',
                options=[{'label': i, 'value': i} for i in fields_set_5],
                placeholder ="Receiver",
                value=None
                
            )
      
        ],
        style={'width': '20%', 'display': 'inline-block'})
# pdb.set_trace()

residuals_pg_dropdown_3=  html.Div([
            dcc.Dropdown(
                id='residuals_pg_dropdown_3',
                # options=[{'label': i, 'value': i} for i in fields_set_6],
                placeholder = "Satellite",
                value=None
                
            )
      
        ],
        style={'width': '20%', 'display': 'inline-block'})
residuals_pg_dropdown_4=  html.Div([
            dcc.Dropdown(
                id='residuals_pg_dropdown_4',
                # options=[{'label': i, 'value': i} for i in fields_set_7],
                placeholder = "Group By",
                value=None
                
            )
      
        ],
        style={'width': '20%', 'display': 'inline-block'})

residuals_pg_dropdown_5=  html.Div([
            dcc.Dropdown(
                id='residuals_pg_dropdown_5',
                options=[{'label': i, 'value': i} for i in fields_set_8],
                placeholder = "Attribute",
                value= None
                
            )
      
        ],
        style={'width': '20%', 'display': 'inline-block'})


#*****************************DROPDOWN  for /page-4 ENDS********************************************************************#:
#***************************************************************************************************************************#:


   
   
        


        


