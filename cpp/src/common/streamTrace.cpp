
#include "streamTrace.hpp"
#include <stdarg.h>
#include <ctype.h>
#include <boost/iostreams/stream.hpp>
#ifndef WIN32
#	include <dirent.h>
#	include <time.h>
#	include <sys/time.h>
#	include <sys/stat.h>
#	include <sys/types.h>
#endif

#include <boost/format.hpp>

#include "observations.hpp"
#include "navigation.hpp"
#include "constants.h"
#include "gaTime.hpp"
#include "common.hpp"

boost::iostreams::stream< boost::iostreams::null_sink > nullStream( ( boost::iostreams::null_sink() ) );


void tracematpde(
	int				level,          ///< []
	std::ostream&	stream,         ///< []
	MatrixXd&		mat,            ///< []
	int				width,          ///< []
	int				precision)		///< []
{
	if (level > level_trace)
		return;

	stream	<< std::endl
			<< std::fixed
			<< std::setw(width)
			<< std::setprecision(precision)
			<< std::endl
			<< mat
			<< std::endl;
}

void tracematpde(
	int				level,      	///< []
	std::ostream&	stream,         ///< []
	VectorXd&		vec,            ///< []
	int				width,          ///< []
	int				precision)      ///< []
{
	if (level > level_trace)
		return;

	stream	<< std::fixed
			<< std::setw(width)
			<< std::setprecision(precision)
			<< vec//.transpose()
			<< std::endl;
}

void tracematpde(
	int				level,     		///< []
	std::ostream&	stream,         ///< []
	MatrixXd*		mat,            ///< []
	int				width,          ///< []
	int				precision)      ///< []
{
	if (level > level_trace)
		return;

	stream	<< std::fixed
			<< std::setw(width)
			<< std::setprecision(precision)
			<< *mat
			<< std::endl;
}

void tracematpde(
	int				level,      	///< []
	std::ostream&	stream,         ///< []
	VectorXd*		vec,            ///< []
	int				width,          ///< []
	int 			precision)      ///< []
{
	if (level > level_trace)
		return;

	stream	<< std::fixed
			<< std::setw(width)
			<< std::setprecision(precision)
			<< vec->transpose()
			<< std::endl;
}

/* fatal error ---------------------------------------------------------------*/
[[deprecated]]
void fatalerr(const char *format, ...)
{
    va_list ap;
    va_start(ap,format); vfprintf(stderr,format,ap); va_end(ap);
    exit(-9);
}

/* print matrix ----------------------------------------------------------------
* print matrix to stdout
* args   : double *A        I   matrix A (n x m)
*          int    n,m       I   number of rows and columns of A
*          int    p,q       I   total columns, columns under decimal point
*         (FILE  *fp        I   output file pointer)
* return : none
* notes  : matirix stored by column-major order (fortran convention)
*-----------------------------------------------------------------------------*/
[[deprecated]]
void matfprint(const double A[], int n, int m, int p, int q, FILE *fp)
{
    int i,j;

    for (i=0;i<n;i++) {
        for (j=0;j<m;j++) fprintf(fp," %*.*f",p,q,A[i+j*n]);
        fprintf(fp,"\n");
    }
}

/* debug trace functions -----------------------------------------------------*/

/* expand file path ------------------------------------------------------------
* expand file path with wild-card (*) in file
* args   : char   *path     I   file path to expand (captal insensitive)
*          char   *paths    O   expanded file paths
*          int    nmax      I   max number of expanded file paths
* return : number of expanded file paths
* notes  : the order of expanded files is alphabetical order
*-----------------------------------------------------------------------------*/
[[deprecated]]
int expath(const char *path, char *paths[], int nmax)
{
    int i,j,n=0;
    char tmp[1024];
#ifdef WIN32
    WIN32_FIND_DATA file;
    HANDLE h;
    char dir[1024]=""; *p;
    trace(3,"expath  : path=%s nmax=%d\n",path,nmax);

    if ((p=strrchr((char *)path,'\\'))) {
        strncpy(dir,path,p-path+1); dir[p-path+1]='\0';
    }
    if ((h=FindFirstFile((LPCTSTR)path,&file))==INVALID_HANDLE_VALUE) {
        strcpy(paths[0],path);
        return 1;
    }
    sprintf(paths[n++],"%s%s",dir,file.cFileName);
    while (FindNextFile(h,&file)&&n<nmax) {
        if (file.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY) continue;
        sprintf(paths[n++],"%s%s",dir,file.cFileName);
    }
    FindClose(h);
#else
    struct dirent *d;
    DIR *dp;
    const char *file=path;
    char dir[1024]="",s1[1024],s2[1024],*p,*q,*r;

//     trace(3,"expath  : path=%s nmax=%d\n",path,nmax);

    if ((p=strrchr((char *)path,'/'))||(p=strrchr((char *)path,'\\'))) {
        file=p+1; strncpy(dir,path,p-path+1); dir[p-path+1]='\0';
    }
    if (!(dp=opendir(*dir?dir:"."))) return 0;
    while ((d=readdir(dp))) {
        if (*(d->d_name)=='.') continue;
        sprintf(s1,"^%s$",d->d_name);
        sprintf(s2,"^%s$",file);
        for (p=s1;*p;p++) *p=(char)tolower((int)*p);
        for (p=s2;*p;p++) *p=(char)tolower((int)*p);

        for (p=s1,q=strtok_r(s2,"*",&r);q;q=strtok_r(NULL,"*",&r)) {
            if ((p=strstr(p,q)))
                p+=strlen(q);
            else break;
        }
        if (p&&n<nmax) sprintf(paths[n++],"%s%s",dir,d->d_name);
    }
    closedir(dp);
#endif
    /* sort paths in alphabetical order */
    for (i=0;i<n-1;i++) {
        for (j=i+1;j<n;j++) {
            if (strcmp(paths[i],paths[j])>0) {
                strcpy(tmp,paths[i]);
                strcpy(paths[i],paths[j]);
                strcpy(paths[j],tmp);
            }
        }
    }
//     for (i=0;i<n;i++) trace(3,"expath  : file=%s\n",paths[i]);

    return n;
}

#ifdef TRACE

int level_trace = 0;       /* level of trace */

void tracelevel(int level)
{
    level_trace = level;
}

void tracepde(int level, FILE *fppde, const char *format, ...)
{
    va_list ap;

    if (!fppde||level>level_trace) return;
    /* traceswap(); */
    fprintf(fppde,"*%d ",level);
    va_start(ap,format); vfprintf(fppde,format,ap); va_end(ap);
    fflush(fppde);
}

void tracepdeex(int level, FILE *fppde, const char *format, ...)
{
    va_list ap;

    if (!fppde||level>level_trace) return;
    /* traceswap(); */
    va_start(ap,format); vfprintf(fppde,format,ap); va_end(ap);
    fflush(fppde);
}

[[deprecated]]
void tracematpde(int level, FILE *fppde, const double *A, int n, int m, int p, int q)
{
    if (!fppde||level>level_trace) return;
    matfprint(A,n,m,p,q,fppde); fflush(fppde);
}

void traceobspde(int level, FILE *fppde, obsd_t *obs, int n)
{
    char str[64],id[16];
    int i,sys;

    if (!fppde||level>level_trace) return;
    for (i=0;i<n;i++) {
        time2str(obs[i].time,str,3);
        obs[i].Sat.getId(id);
		RawSig& sig1 = obs[i].Sigs[F1];
		RawSig& sig2 = obs[i].Sigs[F2];
		RawSig& sig5 = obs[i].Sigs[F5];
		/* E1, E5a, E5b */
		fprintf(fppde," (%2d) %s %-3s rcv %c%c%c%c %13.3f %13.3f %13.3f %13.3f %13.3f %13.3f %d %d %d %3s %3s %3s %4.1f %4.1f %4.1f\n",
		i+1,str,id,
		obs[i].mount[0],obs[i].mount[1],obs[i].mount[2],obs[i].mount[3],
		sig1.L,sig2.L,sig5.L,
		sig1.P,sig2.P,sig5.P,
		sig1.LLI,sig2.LLI,sig5.LLI,
		obscodes[sig1.code].c_str(),
		obscodes[sig2.code].c_str(),
		obscodes[sig5.code].c_str(),
		sig1.snr*0.25,sig2.snr*0.25,sig5.snr*0.25);
    }
    fflush(fppde);
}

void traceobspde(int level, Trace& trace, obsd_t *obs, int n)
{
    char str[64],id[16];
    int i,sys;

    if (level>level_trace) return;
    for (i=0;i<n;i++)
	{
        time2str(obs[i].time,str,3);
        obs[i].Sat.getId(id);
		RawSig& sig1 = obs[i].Sigs[F1];
		RawSig& sig2 = obs[i].Sigs[F2];
		RawSig& sig5 = obs[i].Sigs[F5];
		/* E1, E5a, E5b */
		tracepde(level, trace," (%2d) %s %-3s rcv %c%c%c%c %13.3f %13.3f %13.3f %13.3f %13.3f %13.3f %d %d %d %3s %3s %3s %4.1f %4.1f %4.1f\n",
		i+1,str,id,
		obs[i].mount[0],
		obs[i].mount[1],
		obs[i].mount[2],
		obs[i].mount[3],
		sig1.L,							sig2.L,							sig5.L,
		sig1.P,							sig2.P,							sig5.P,
		sig1.LLI,						sig2.LLI,						sig5.LLI,
		obscodes[sig1.code].c_str(),	obscodes[sig2.code].c_str(),	obscodes[sig5.code].c_str(),
		sig1.snr*0.25,					sig2.snr*0.25,					sig5.snr*0.25);
    }
    trace << std::flush;
}

void tracenavpde(int level, FILE *fppde, const nav_t *nav)
{
    char s1[64],s2[64],id[16];
    int prn;

    if (!fppde||level>level_trace) return;

	int i = 0;
	for (auto& [a, ephList]	: nav->ephMap)
	for (auto& eph 			: ephList)
	{
        i++;
		{
            time2str(eph.toe,s1,0);
            time2str(eph.ttr,s2,0);
            eph.Sat.getId(id);
            fprintf(fppde,"(%3d) %-3s : %s %s %3d %3d %08x\n",i,id,s1,s2,eph.iode,eph.iodc,eph.svh);
        }
    }
    i = 0;
	for (auto& [a, gephList]	: nav->gephMap)
	for (auto& geph 			: gephList)
	{
		i++;
		{
            time2str(geph.toe,s1,0);
            time2str(geph.tof,s2,0);
            geph.Sat.getId(id);
            fprintf(fppde,"(%3d) %-3s : %s %s %3d %3d %3d %8.3f\n",i,
                    id,s1,s2,geph.frq,geph.iode,geph.svh,geph.taun*1E6);
        }
    }

    fprintf(fppde,"(GPS ion) %9.4e %9.4e %9.4e %9.4e\n",nav->ion_gps[0],
            nav->ion_gps[1],nav->ion_gps[2],nav->ion_gps[3]);
    fprintf(fppde,"(GPS ion) %9.4e %9.4e %9.4e %9.4e\n",nav->ion_gps[4],
            nav->ion_gps[5],nav->ion_gps[6],nav->ion_gps[7]);
    fprintf(fppde,"(GAL ion) %9.4e %9.4e %9.4e %9.4e\n",nav->ion_gal[0],
            nav->ion_gal[1],nav->ion_gal[2],nav->ion_gal[3]);
}
/* End of ACS PDE debug */

#else
void tracelevel(int level) {}

void traceclosepde(FILE *fppde);
void tracepde(int level, FILE *fppde, const char *format,...);
void tracematpde(int level, FILE *fppde, const double *A, int n, int m, int p, int q);
void traceobspde(int level, FILE *fppde, const obsd_t *obs, int n);
void tracenavpde(int level, FILE *fppde, const nav_t *nav);

#endif /* TRACE */
