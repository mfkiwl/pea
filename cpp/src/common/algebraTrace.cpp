
#include <iostream>
#include <fstream>
#include <map>

using std::map;

#include "eigenIncluder.hpp"

#include "algebraTrace.hpp"
#include "navigation.hpp"
#include "algebra.hpp"

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/binary_object.hpp>
#include <boost/serialization/map.hpp>

/** Returns the type of object that is located at the specified position in a file
 */
E_SerialObject getFilterTypeFromFile(
	long int& startPos,	///< Position of object
	string filename)	///< Path to archive file
{
	std::fstream fileStream(filename, std::ifstream::binary | std::ifstream::in);

	if (!fileStream)
	{
		return E_SerialObject::NONE;
	}

	binary_iarchive serial(fileStream, 1); //no header

	long int itemPosition;

	if (startPos < 0)	{	fileStream.seekg(			-sizeof(itemPosition),	fileStream.end);	}
	else				{	fileStream.seekg(startPos	-sizeof(itemPosition),	fileStream.beg);	}

	serialize(serial, itemPosition);
	fileStream.seekg(itemPosition, fileStream.beg);

	int type_int;
	serialize(serial, type_int);
	E_SerialObject type = E_SerialObject::_from_integral(type_int);

	return type;
}

/** Initialises the outputs for input/output of filter states during (re)processing
 */
void initFilterTrace(
	KFState&	kfState,		///< Filter object to apply to
	string		traceFilename,	///< Filename to store/read data from
	int			rts_lag)		///< Number of epochs to lag when doing RTS smoothing (-ve => complete reverse)
{
	kfState.rts_forward_filename	= traceFilename + "_forward";
	kfState.rts_reverse_filename	= traceFilename + "_reverse";
	kfState.rts_smoothed_filename	= traceFilename + "_smoothed";
	kfState.rts_lag					= rts_lag;

	std::ofstream ofs1(kfState.rts_forward_filename,	std::ofstream::out | std::ofstream::trunc);
	std::ofstream ofs2(kfState.rts_reverse_filename,	std::ofstream::out | std::ofstream::trunc);
	std::ofstream ofs3(kfState.rts_smoothed_filename,	std::ofstream::out | std::ofstream::trunc);
}

void inputPersistantNav(string filename)
{
	long int startPos = -1;
	E_SerialObject type = getFilterTypeFromFile(startPos, filename);
	if (type == +E_SerialObject::NAVIGATION_DATA)
	{
		getFilterObjectFromFile(E_SerialObject::NAVIGATION_DATA, nav, startPos, filename);
	}
}

void outputPersistantNav(string filename)
{
	{
		std::fstream fileStream(filename, std::ifstream::binary | std::ifstream::out);
	}
	spitFilterToFile(nav, E_SerialObject::NAVIGATION_DATA, filename);
}
