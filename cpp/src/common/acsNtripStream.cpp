
#include "acsNtripStream.hpp"

#include <boost/system/error_code.hpp>

using namespace boost::system;

#include <list>

using std::list;

void NtripStream::connect()
{
	std::cout << "(Re)connecting " << url.sanitised() << std::endl;

	try
	{
		// Get a list of endpoints corresponding to the server name.
		tcp::resolver				resolver(io_service);
// 			tcp::resolver::query		query(url.host, url.port_str);		//slow
		tcp::resolver::query		query(boost::asio::ip::tcp::v4(), url.host, url.port_str);
		tcp::resolver::iterator		endpoint_iterator = resolver.resolve(query);

// 		socket.set_option(boost::asio::detail::socket_option::integer<SOL_SOCKET, SO_RCVTIMEO>{ 1000 });

		// Try each endpoint until we successfully establish a connection.
		B_asio::connect(socket, endpoint_iterator);
		B_asio::streambuf	request;
		std::ostream			request_stream(&request);

									request_stream	<< "GET " 		<< url.path << " HTTP/1.0\r\n";
									request_stream	<< "User-Agent: NTRIP ACS/1.0\r\n";
									request_stream	<< "Host: " 	<< url.host;
		if (!url.port_str.empty())	request_stream	<< ":" 			<< url.port_str;
									request_stream	<< "\r\n";
		if (!url.username.empty())	request_stream	<< "Authorization: Basic "
													<< Base64::encode(string(url.username + ":" + url.password))
													<< "\r\n";
									request_stream	<< "\r\n";

		// Send the request.
		B_asio::write(socket, request);

		// Read the response status line. The response streambuf will automatically
		// grow to accommodate the entire line. The growth may be limited by passing
		// a maximum size to the streambuf constructor.

		B_asio::streambuf	response;
		B_asio::read_until(socket, response, "\r\n");

		std::istream	response_stream(&response);

		while (1)
		{
			string line;
			std::getline(response_stream, line);
			boost::algorithm::trim_right(line);
			if (line.empty())
			{
				break;
			}

			lastDataTime = system_clock::now();
// 				BOOST_LOG_TRIVIAL(debug)
// 				<< "Read line [" << line.length()
// 				<< "]=[" << line << "]";
		}

		// the timeout value
		unsigned int timeout_milli = 1000;

		struct timeval tv;
		tv.tv_sec	= timeout_milli / 1000;
		tv.tv_usec	= (timeout_milli % 1000) * 1000;
		setsockopt(socket.native_handle(), SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
		setsockopt(socket.native_handle(), SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv));
		boost::asio::socket_base::keep_alive option(true);

		socket.set_option(option);
	}
	catch (std::exception& e)
	{
		std::cout << "Exception: " << e.what() << "\n";
	}
}

void NtripStream::getData()
{
	boost::system::error_code	error;

	list<vector<char>>	responseList;
	int totalCount = 0;
	while (1)
	{
		if (!socket.available(error))
		{
// 			if (error)
// 				std::cout << error.category().name() << error.category().message(error.value()) << '\n';

			if (system_clock::now() > lastDataTime + std::chrono::milliseconds(timeout * 1000))
			{
// 				std::cout << "Timeout" << std::endl;
				lastDataTime = system_clock::now();
				connect();
			}

			break;
		}


		B_asio::streambuf	response;
		vector<char> vecc;
		vecc.resize(1024);
		int count = boost::asio::read(socket, boost::asio::buffer(vecc.data(), vecc.size()), boost::asio::transfer_at_least(1), error);
		if (count == 0)
		{
			break;
		}
		vecc.resize(count);

		lastDataTime = system_clock::now();

		totalCount += count;
		responseList.push_back(vecc);
// 		if (error != boost::asio::error::eof)
// 			throw boost::system::system_error(error);
	}
	receivedData.reserve(totalCount);

	for (auto& a : responseList)
	{
		receivedData.insert(receivedData.end(), a.begin(), a.end());
	}
}
