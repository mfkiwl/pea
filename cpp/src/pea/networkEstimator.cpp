
#include <iostream>
#include <vector>

#include "networkEstimator.hpp"
#include "algebraTrace.hpp"
#include "streamTrace.hpp"
#include "navigation.hpp"
#include "testUtils.hpp"
#include "acsStream.hpp"
#include "acsConfig.hpp"
#include "station.hpp"
#include "algebra.hpp"
#include "common.hpp"
#include "orbits.hpp"
#include "enums.h"

#include "eigenIncluder.hpp"

/** Remove ambiguity states from filter when they are not measured for an epoch.
 * This effectively reinitialises them on the following epoch as a new state, and can be used for simple
 * resolution of cycle-slips
 */
void removeUnmeasuredAmbiguities(
	KFState&			kfState, 			///< Filter to remove states from
	map<KFKey, bool>	measuredStates)		///< Map of measured states in this epoch to compare against.
{
	for (auto it = kfState.stateTransitionMap.cbegin(); it != kfState.stateTransitionMap.cend();  )
	{
		KFKey key = it->first;

		if	( (key.type == KF::AMBIGUITY)
			&&(measuredStates[key] == false))
		{
			kfState.procNoiseMap.		erase(key);
			kfState.initNoiseMap.		erase(key);
			kfState.stateClampMaxMap.	erase(key);
			kfState.stateClampMinMap.	erase(key);
			kfState.rateTransitionMap.	erase(key);

			it = kfState.stateTransitionMap.erase(it);
		}
		else
		{
			++it;
		}
	}
}


Matrix3d stationEopPartials(
	Vector3d&	rRec)
{
	const double radsPerMas = PI / (180 * 60 * 60 * 1000);

	Matrix3d partials;
	auto& X = rRec(0);
	auto& Y = rRec(1);
	auto& Z = rRec(2);
	partials(0,0) = +Z * radsPerMas;	//dx/dxp		= dx/dRotY
	partials(0,1) =  0;					//dy/dxp		= dy/dRotY
	partials(0,2) = -X * radsPerMas;	//dz/dxp		= dz/dRotY

	partials(1,0) =  0;					//dx/dyp		= dx/dRotX
	partials(1,1) = -Z * radsPerMas;	//dy/dyp		= dy/dRotX
	partials(1,2) = +Y * radsPerMas;	//dz/dyp		= dz/dRotX

	partials(2,0) = +Y * radsPerMas;	//dx/dut1		= dx/dRotZ
	partials(2,1) = -X * radsPerMas;	//dy/dut1		= dy/dRotZ
	partials(2,2) =  0;					//dz/dut1		= dz/dRotZ

	return partials;
}

/** Check and correct clock jitter/wraparound
 */
void correctRecClocks(
	KFState&	kfState,	///< Filter to correct clock estimates in
	Station*	refRec)		///< Reference clock to use as basis for adjusting others
{
	for (auto& [key, index] : kfState.kfIndexMap)
	{
		if	( (key.type			!= KF::REC_SYS_BIAS)
			||(key.num			!= 0)
			||(key.station_ptr	== nullptr))
		{
			continue;
		}

		auto& rec		= *key.station_ptr;
 		auto& recOpts	= acsConfig.getRecOpts(key.str);

		double wraparound_distance	= CLIGHT * 1e-3;
		double wraparound_tolerance	= CLIGHT * acsConfig.clock_wrap_threshold;

		double deltaBias		= (rec.rtk.sol.dtRec_m			[0] - refRec->rtk.sol.dtRec_m[0]);
		double previousDelta	=  rec.rtk.sol.deltaDt_net_old	[0];

		double deltaDelta = deltaBias - previousDelta;

		if (recOpts.clk_rate.estimate == false)
		{
			//get, modify and set the old bias in the state according to SPP estimates
			double oldBias = 0;
			kfState.getKFValue(key, oldBias);
			oldBias += deltaDelta;
			kfState.setKFValue(key, oldBias);
		}
		else if	( (rec.rtk.sol.deltaDt_net_old[0] == 0)
				||( abs(deltaDelta) > wraparound_distance - wraparound_tolerance
				  &&abs(deltaDelta) < wraparound_distance + wraparound_tolerance))
		{
			//get, modify and set the old bias in the state
			double oldBias = 0;
			kfState.getKFValue(key, oldBias);
			if (deltaDelta > 0)	{	oldBias += wraparound_distance;	}
			else				{	oldBias -= wraparound_distance;	}
			kfState.setKFValue(key, oldBias);
		}

		//store this value here for next time
		rec.rtk.sol.deltaDt_net_old[0] = deltaBias;
	}
}

/** Estimates parameters for a network of stations simultaneously
 */
int networkEstimator(
	Trace&			trace,			///< Trace to output to
	StationList&	stations,       ///< List of stations containing observations for this epoch
	KFState&		kfState,        ///< Kalman filter object containing the network state parameters
	double			tgap)           ///< The time gap since last epoch
{
	TestStack ts(__FUNCTION__);

	kfState.initFilterEpoch();

	//count the satellites common between receivers
	int total = 0;
	std::map<int, int> satCountMap;
	for (auto& rec : stations)
	{
		int count = 0;
		for (auto& obs : rec->obsList)
		{
			if (acsConfig.process_sys[obs.Sat.sys] == false)
			{
				continue;
			}

			satCountMap[obs.Sat]++;
			count++;
		}

		total+= count;
	}
	string recString;
	for (auto& rec_ptr : stations)
	{
		auto& recOpts = acsConfig.getRecOpts(rec_ptr->id);

		if	(recOpts.exclude)
			continue;

		recString += rec_ptr->id + ",";
	}
																								TestStack::testStr("recString", recString);

	KFMeasEntryList		kfMeasEntryList;
	map<KFKey, bool>	measuredStates;
	Station*	refRec = nullptr;
	bool		refClk = false;

	string obsString;

	for (auto& rec_ptr		: stations)  														if (obsString += rec_ptr->id, true)
	for (auto& obs 			: rec_ptr->obsList)
	for (auto& [ft, sig] 	: obs.Sigs)
	{
		auto& rec = *rec_ptr;
		auto& satOpts = acsConfig.getSatOpts(obs.Sat);
		auto& recOpts = acsConfig.getRecOpts(rec.id);

		int count = satCountMap[obs.Sat];

		if	( (count < 2)
			||(obs.		exclude)
			||(satOpts.	exclude)
			||(recOpts.	exclude)
			||(sig.vsig == false)
			||(ft != FTYPE_IF12))		//only use IF linear combinations for now
		{
			continue;
		}
																								obsString += "," + obs.Sat.id();

		if	(    refRec					== nullptr
			&&( acsConfig.pivot_station	== rec.id
			  ||acsConfig.pivot_station	== "<AUTO>"))
		{
			//use this receiver as the reference receiver for clock offsets
			refRec = &rec;
		}

		ObsKey obsKeyCode = {	obs.Sat, rec.id, "P"	};
		ObsKey obsKeyPhas = {	obs.Sat, rec.id, "L"	};

		KFMeasEntry	codeMeas(&kfState, obsKeyCode);
		KFMeasEntry	phasMeas(&kfState, obsKeyPhas);

		codeMeas.metaDataMap["obs_ptr"] = &obs;
		phasMeas.metaDataMap["obs_ptr"] = &obs;

		SatStat& satStat = *obs.satStat_ptr;

		//initialise this rec/sat's measurements
		codeMeas.setValue(sig.codeRes);
		phasMeas.setValue(sig.phasRes);

		/* stochastic model (IF LC to be refined) */
		codeMeas.setNoise(sig.codeVar);
		phasMeas.setNoise(sig.phasVar);

		//initialise this rec/sat's design matrix

		//create some keys for use below
		//they determine which states the measurements are applied to
		//in some cases they are per receiver, some states are per rec/sat pair etc..
							//type					sat			receiver
		KFKey satClockKey		=	{KF::SAT_CLOCK,			obs.Sat};
		KFKey satClockRateKey	=	{KF::SAT_CLOCK_RATE,	obs.Sat};
		KFKey ambiguityKey		=	{KF::AMBIGUITY,			obs.Sat,	rec.id, 0,								&rec	};	//todo aaron, does there need to be a reference satellite clock to prevent all biases walking off?
		KFKey refClockKey		=	{KF::REF_SYS_BIAS,		{},			rec.id,	SatSys(E_Sys::GPS).biasGroup(),	&rec	};
		KFKey recClockKey		=	{KF::REC_SYS_BIAS,		{},			rec.id,	SatSys(E_Sys::GPS).biasGroup(),	&rec	};
		KFKey recClockRateKey	=	{KF::REC_SYS_BIAS_RATE,	{},			rec.id,	SatSys(E_Sys::GPS).biasGroup(),	&rec	};
		KFKey recSysBiasKey		=	{KF::REC_SYS_BIAS,		{},			rec.id, obs.Sat.biasGroup(),			&rec	};
		KFKey recPosKeys[3];
		KFKey tropKeys	[3];
		KFKey eopKeys	[3]		= {	{KF::EOP,				{},			"XP"},
									{KF::EOP,				{},			"YP"},
									{KF::EOP,				{},			"UT1"}	};
		for (short i = 0; i < 3; i++)
		{
			recPosKeys[i]	= {KF::REC_POS,			{},			rec.id,	i,				&rec	};
		}
		for (short i = 0; i < 3; i++)
		{
			tropKeys[i]		= {KF::TROP,			{},			rec.id,	i,				&rec	};
		}

		//find approximation of ambiguity using L-P
		double amb = sig.phasRes - sig.codeRes;

		//add the elements in the design matrix.
		//if they use a state parameter that hasn't been used before, it will be created and initialised

		//all obs have GPS clock bias
		if (recOpts.clk.estimate)
		if (&rec != refRec)
		{
			InitialState init		= initialStateFromConfig(recOpts.clk);
			codeMeas.addDsgnEntry(recClockKey,		+1,					init);
			phasMeas.addDsgnEntry(recClockKey,		+1,					init);

			if (recOpts.clk_rate.estimate)
			{
				InitialState init	= initialStateFromConfig(recOpts.clk_rate);
				kfState.setKFTransRate(recClockKey, recClockRateKey,	1, init);
			}
		}
		else
		{
			if (refClk == false)
			{
				refClk = true;

				KFMeasEntry	pseudoMeas(&kfState);	//todo aaron, add pseudomeasurements to set the reference receiver to 0 rather than being blank?
				pseudoMeas.setValue(0);				//this works, but is it general? what happens after the first epoch?
				pseudoMeas.setNoise(0.000001);

				InitialState init		= {0, SQR(0.0001), SQR(0)};
				pseudoMeas.addDsgnEntry(refClockKey,	+1,					init);
				kfMeasEntryList.push_back(pseudoMeas);
			}
		}

		//other systems may have inter-system bias too.
		if (recOpts.clk.estimate)
		if (obs.Sat.sys != +E_Sys::GPS)
		{
			InitialState init		= initialStateFromConfig(recOpts.clk);
			codeMeas.addDsgnEntry(recSysBiasKey,	+1,						init);
			phasMeas.addDsgnEntry(recSysBiasKey,	+1,						init);
		}

		if (recOpts.pos.estimate)
		for (int i = 0; i < 3; i++)
		{
			InitialState init		= initialStateFromConfig(recOpts.pos, i);
			codeMeas.addDsgnEntry(recPosKeys[i],	-satStat.e[i], 			init);
			phasMeas.addDsgnEntry(recPosKeys[i],	-satStat.e[i], 			init);
		}

		if (recOpts.trop.estimate)
		{
			InitialState init		= initialStateFromConfig(recOpts.trop);
			codeMeas.addDsgnEntry(tropKeys[0],		satStat.mapWet,			init);
			phasMeas.addDsgnEntry(tropKeys[0],		satStat.mapWet,			init);
		}

		if (recOpts.trop_grads.estimate)
		for (int i = 0; i < 2; i++)
		{
			InitialState init	= initialStateFromConfig(recOpts.trop_grads, i);
			codeMeas.addDsgnEntry(tropKeys[i+1],	satStat.mapWetGrads[i],	init);
			phasMeas.addDsgnEntry(tropKeys[i+1],	satStat.mapWetGrads[i],	init);
		}

		if (satOpts.clk.estimate)
		{
			InitialState init		= initialStateFromConfig(satOpts.clk);
			codeMeas.addDsgnEntry(satClockKey,		-1,						init);
			phasMeas.addDsgnEntry(satClockKey,		-1,						init);

			if (satOpts.clk_rate.estimate)
			{
				InitialState satClkRateInit	= initialStateFromConfig(satOpts.clk_rate);
				kfState.setKFTransRate(satClockKey, satClockRateKey,	1,	satClkRateInit);
			}
		}

		if (recOpts.amb.estimate)
		{
			InitialState init		= initialStateFromConfig(recOpts.amb);
			init.x = amb;
			phasMeas.addDsgnEntry(ambiguityKey,		+1,						init);
			measuredStates[ambiguityKey] = true;
		}

		if (satOpts.orb.estimate)
		{
			/* transmission time by satellite clock */
			auto& [ft, sig] = *obs.Sigs.begin();
			double pr = sig.P;
			gtime_t time = timeadd(obs.time, - pr / CLIGHT);

			bool pass = orbPartials(trace, time, obs, obs.satPartialMat);
			if (pass == false)
			{
				continue;
			}

			VectorXd orbitPartials = obs.satPartialMat * satStat.e;

			for (int i = 0; i < obs.satOrb_ptr->numUnknowns; i++)
			{
				string name = obs.satOrb_ptr->parameterNames[i];
				KFKey orbPtKey	= {KF::ORBIT_PTS,	obs.Sat,	std::to_string(100 + i).substr(1) + "_" + name};

				InitialState init	= initialStateFromConfig(satOpts.orb, i);
				codeMeas.addDsgnEntry(orbPtKey,		orbitPartials(i),			init);
				phasMeas.addDsgnEntry(orbPtKey,		orbitPartials(i),			init);
			}
		}

		if (acsConfig.netwOpts.eop.estimate)
		{
			Matrix3d partialMatrix	= stationEopPartials(rec.station.rRec);
			Vector3d eopPartials	= partialMatrix * satStat.e;

			for (int i = 0; i < 3; i++)
			{
				InitialState init	= initialStateFromConfig(acsConfig.netwOpts.eop, i);
				codeMeas.addDsgnEntry(eopKeys[i],	eopPartials(i),				init);
				phasMeas.addDsgnEntry(eopKeys[i],	eopPartials(i),				init);
			}
		}

		kfMeasEntryList.push_back(codeMeas);
		kfMeasEntryList.push_back(phasMeas);
	}

	//if not enough data is available, return early
	if (refRec == nullptr)
	{
		return 0;
	}


 	removeUnmeasuredAmbiguities(kfState, measuredStates);

	//add process noise to existing states as per their initialisations.
	kfState.stateTransition(trace, tgap);

	//combine the measurement list into a single matrix
	KFMeas combinedMeas = kfState.combineKFMeasList(kfMeasEntryList);
	combinedMeas.time = stations.front()->obsList.front().time;

	correctRecClocks(kfState, refRec);
																								TestStack::testStr("obsString", obsString);
																								TestStack::testMat("A", combinedMeas.A, 1e-3);
																								TestStack::testMat("Y",	combinedMeas.Y);

	//if there are uninitialised state values, estimate them using least squares

	if (kfState.lsqRequired)
	{
		kfState.lsqRequired = false;
		trace << "\r\n -------INITIALISING NETWORK USING LEAST SQUARES--------" << std::endl;

 		kfState.leastSquareInitStates(trace, combinedMeas);
	}

	if (acsConfig.netwOpts.filter_mode == E_FilterMode::LSQ)
	{
		trace << "\r\n -------DOING NETWORK LEAST SQUARES--------" << std::endl;

		kfState.filterLeastSquares(trace, combinedMeas);
	}
	else
	{
		trace << "\r\n -------DOING NETWORK KALMAN FILTER --------" << std::endl;

		kfState.filterKalman(trace, combinedMeas, false);
	}
																								TestStack::testMat("aX2",			kfState.x, 	0, 		&kfState.P);
																								TestStack::testMat("PpostUpdate3",	kfState.P, 	5e-3);

	return 0;
}
