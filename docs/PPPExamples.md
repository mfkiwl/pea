# Precise Point Positioning Examples (PPP)

## Using the Ionosphere-free observable to process a static data set

In this example we will process 24 hours of data from a permanent reference frame station. The algorithm that will be use an L1+L2 and L1+L5 ionosphere free combination.
The log files and processing results can be found in `/data/acs/pea/output/exs/EX01_IF/`.

    $ ./pea --config ../../config/EX01-IF-PPP.yaml

The pea will then have the following output in in /data/acs/pea/output/exs/EX01\_IF/ :

EX01\_IF20624.snx              - contains the station position estimates in SINEX format
EX01\_IF-ALIC2019199900.TRACE  - contains the logging information from the processing run

    $ grep "REC_POS" /data/acs/pea/output/exs/EX01_IF/EX01_IF-ALIC201919900.TRACE > ALIC_201919900.PPP

    This will pipe all of the receiver position results reported in the station trace file to a seperate file for plotting.

    $ python3 /data/acs/pea/python/source/pppPlot.py --ppp /data/acs/pea/output/exs/EX01_IF/ALIC_201919900.PPP

This will then create the plots alic\_pos.png, a time series of the difference between the estimated receiver position and the median estimated position.
And the plot alic\_snx\_pos.png, a time series of the difference between the estimated receiver position and the IGS SINEX solution for Alic Springs on this day.

## Single Frequency Processing 

    $ ./pea --config ../../config/Ex01-SF-PPP.yaml

    $ grep "\$POS" /data/acs/pea/output/exs/EX01_SF/EX01_SF-ALBY202011500.TRACE

And you should see something similar to the following:
<snip>
$POS,2062,431940.000,0,-4052052.7956,4212836.0144,-2545104.6423,0.00000043966020,0.00000039738502,0.00000013421476
$POS,2062,431970.000,0,-4052052.7956,4212836.0144,-2545104.6423,0.00000043965772,0.00000039738393,0.00000013421667


