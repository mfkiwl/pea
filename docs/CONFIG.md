# Description of Configuration File options for the PEA 

## Overview

The `pea` uses a yamli formatted configuration file to control which processing mode is used and which options are enabled.
A set of example configuration files are provided in the subdirectory config/ whihc demonstrated how to enable different processing options for the pea.

The configuration file is broken into several sections. 

1. input\_files
2. station\_data
3. output\_files
4. output\_options
5. processing\_options
6. user\_filter\_parameters
7. default\_filter\_parameters
8. override\_filter\_parameters

The sections that are required are dependent upon the processing mode you which to run. A breakdown of which sections are required for each procesisng mode is given further below.

## input\_files options

### root\_input\_directory
The directory path where all of the products and input files required by the pea are stored. 

Input Options:

This can be provided as a complete absolute directory path, alternatively it can be provided as a relative path (from where the program is executed)

Default: /data/acs/pea/proc/exs/products/
 
### antenna

The file name of the absolute antenna calibration file in ANTEX format. This can be obtained from ftp://igs.org/pub/station/general . To get the latest version igs14.atx which will have the latest satellite antenna calibration values. 
An archive of previous ANTEX files is kept on ftp://igs.org/pub/station/general/pcv\_archive/.

Default: igs14.atx

### blqfile

The name of the file that contains a table of the ocean loading coefficients for your stations. One possible source can be obtained from http://holt.oso.chalmers.se/loading/.  

Default: OLOAD.BLQ

### navigation

Broadcast navigation file that can be obtained from most global data centres. It is better to use an accumulated broadcast file such as the one obtained at cddis
e.g. ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/YYYY/brdc/, where YYYY is the year.

Default: brdcDDD.YYn


### sp3file

A precise orbit file containing the satellite orbit files, these are typically available from IGS global data centres,
e.g. ftp://cddis.gsfc.nasa.gov/pub/gps/products/WWWW/ , where WWWW is the gps week.


### erpfile

Generally not required by the PEA as all of the calculations are computed in the terrestial reference frame.

### dcbfile

A file containing the differential correction biases.

### clkfile

The satellite and clock estimates in clock RINEX format provided by an analysis centre or a combined IGS products.

## station\_data

### root\_stations\_directory

The directory path where all of the RINEX3 or RINEX2 observation data is located that needs to be processed by the pea. 

Input Options:

This can be provided as a complete absolute directory path, alternatively it can be provided as a relative path (from where the program is executed)

### files:

Input Options:

1. provide a list of rinex files
    - RINEX_FILE_number1.rnx
    - RINEX_FILE_number2.rnx
    - RINEX_FILE_number3.rnx
2. <SEARCH_DIRECTORY> and all of the RINEX files in the directory will be used.
     -<SEARCH_DIRECTORY>

## output\_files

These options control what files will be output by the PEA and the level of debugging information they will contain.

### root\_output\_directory

The directory path the out put files will be placed 

### log_level:                      info                             # debug, info, warn, error as defined in boost::log
A heirarchial level of Logging. 
The Input Options are : 
1. debug - everything
2. info - only info messages with warning and error messages
3. warn - warning and error messages
4. error - just the errors
### log\_directory:                  ./
### log\_filename:                   <CONFIG>-<YYYY><DDD><HH>.LOG

### output\_trace:                   true
### trace\_level:                    2
### trace\_directory:                ./
### trace\_filename:                 <CONFIG>-<STATION><YYYY><DDD><HH>.TRACE

### output\_residuals:               true
### residuals\_directory:            ./
### residuals\_filename:             <CONFIG>-<YYYY><DDD><HH>.RES


## output\_options

### config\_description:             EX01_IF
### analysis\_agency:                GAA
### analysis\_center:                Geoscience Australia
### analysis\_program:               AUSACS
### rinex\_comment:                  AUSNETWORK1

## processing\_options

### start\_epoch:                2019-07-18 00:00:00

The first epoch to start the processing at. The default is the first epoch read from the RINEX file. 
The format is YYYY-MM-DD hh:mm:ss e.g. 2019-07-18 00:00:00

### end\_epoch:                  2019-07-18 23:59:30

The epoch to stop the processing at. The default is the last epoch read from the RINEX file. 
The format is YYYY-MM-DD hh:mm:ss e.g. 2019-07-18 23:59:30

### epoch\_interval:             30          #seconds

The cadence of observations to be passed into the estimation engine in seconds. Any other extra observatons will just be skipped over.

## process\_modes:

These options define how the pea will process

### user:                   true

true - will run the pea in PPP mode

### network:                false

true - will set up the pea to run in network mode, this is needed to estimate the satelliteorbit and clock values

### minimum_constraints:    false

A true value will set up a minimum constrain value to be applied as specified in the option below

### rts:                    false

Do a backwards processing run to smooth the estimates. This will only smooth estimates which have had a process noise applied to it.

### ionosphere:             false

Estimate an ionosphere model.

## process_sys:

This section specifies what constellation to process, set as true or false, at the moment the PEA can only process one
constellation at a time.

### gps:            true

True -> process GPS observations

### glo:            false

True -> process GLONASS observations 

### gal:            false

True -> process Galileo observations

### bds:            false

True -> process Beidou observations

### elevation_mask:     7   #degrees

The elevation mask to apply to the observations, any observations below this will be ignored.

### tide_solid:         true

If true apply the solid earth tide

### tide_pole:          true

If true apply the pole tide model

### tide_otl:           true

If true apply the ocean tide loading correction

### phase\_windup:       true
### reject\_eclipse:     true            #  reject observation during satellite eclipse periods
### raim:               true
### antexacs:           true

## cycle\_slip:
### thres\_slip: 0.05

### max\_inno:   0
### max\_gdop:   30

## troposphere:
### model:      vmf3    #gpt2
### vmf3dir:    grid5/
### orography:  orography\_ell\_5x5
        # gpt2grid: EX03/general/gpt_25.grd

## pivot\_station:        "USN7"
## pivot\_satellite:      "G01"

## user\_filter\_parameters

    max_filter_iterations:      1
    max_filter_removals:        0

    rts_lag:                    -1      #-ve for full reverse, +ve for limited epochs
    rts_directory:              ./
    rts_filename:               PPP-<CONFIG>-<STATION>.rts

    inverter:                   LLT         #LLT LDLT INV


## default\_filter\_parameters

This is where the default models and parameters are set for all of the data to be processed. They can be overrided later if necessary, in the override setion.

###    stations:

####        error\_model:        elevation\_dependent         #uniform elevation_dependent
The error model defines which weighting scheme is applied to the code and phase observations. The weighting models which are available are:

    uniform - all observations are given the same weight
    elevation_dependent - observations are given a weight based on their elevation angle. The formula used is code_sigmas[0] / sin(elevation)
 
####        code\_sigmas:        [0.15, 1]


####        error\_ratio:        [100, 150, 150]

This is the error\_ration[1] is used in PPP . 


        pos:
            estimated:          true
            sigma:              [0.1]
            proc_noise:         [0.001]
            proc_noise_dt:      hour
            #apriori:                                   # taken from other source, rinex file etc.
            #frame:              xyz #ned
            #proc_noise_model:   Gaussian

        clk:
            estimated:          true
            sigma:              [0]
            proc_noise:         [1.8257418583505538]
            #proc_noise_model:   Gaussian

        clk_rate:
            #estimated:          true
            sigma:              [500]
            proc_noise:         [1e-4]
            clamp_max:          [+500]
            clamp_min:          [-500]

        amb:
            estimated:          true
            sigma:              [60]
            proc_noise:         [0]
            #proc_noise_dt:      day
            #proc_noise_model:   RandomWalk

        trop:
            estimated:          true
            sigma:              [0.1]
            proc_noise:         [0.3]
            proc_noise_dt:      hour
            #proc_noise_model:   RandomWalk

        trop_grads:
            estimated:          true
            sigma:              [1]
            proc_noise:         [0]
            #proc_noise_dt:      hour
            #proc_noise_model:   RandomWalk

    satellites:

        clk:
            estimated:          true
            sigma:              [0]
            proc_noise:         [0.03651483716701108]
            #proc_noise_dt:      min
            #proc_noise_model:   RandomWalk

        clk_rate:
            #estimated:          true
            sigma:              [500]
            proc_noise:         [1e-4]
            clamp_max:          [+500]
            clamp_min:          [-500]

        orb:
            estimated:          false


## override\_filter\_parameters

    stations:

            #ALIC:
            #pos:
            #    sigma:              [60]

                #proc_noise:         [0]
                ##proc_noise_model:   Gaussian
            #clk:
                #sigma:              [0.01]

        #AGGO:
            #exclude: true
        #ALIC:
            #exclude: true
        #ANKR:
            #exclude: true
                #network_estimated:  false

## Command line overrides

In this section we give a brief overview on how the command line options can be used to override the configuration file settings.

A list of available options is provided by entering the following command:

    $ ./pea --help

	Options:
	--help                      Help
	--verbose                   More output
	--quiet                     Less output
	--config arg                Configuration file
	--trace_level arg           Trace level
	--antenna arg               ANTEX file
	--navigation arg            Navigation file
	--sinex arg                 SINEX file
	--sp3file arg               Orbit (SP3) file
	--clkfile arg               Clock (CLK) file
	--dcbfile arg               Code Bias (DCB) file
	--ionfile arg               Ionosphere (IONEX) file
	--podfile arg               Orbits (POD) file
	--blqfile arg               BLQ (Ocean loading) file
	--erpfile arg               ERP file
	--elevation_mask arg        Elevation Mask
	--max_epochs arg            Maximum Epochs
	--epoch_interval arg        Epoch Interval
	--rnx arg                   RINEX station file
	--root_input_dir arg        Directory containg the input data
	--root_output_directory arg Output directory
	--start_epoch arg           Start date/time
	--end_epoch arg             Stop date/time
	--dump-config-only          Dump the configuration and exit

### --start_epoch <arg>

Specify the start time for processing to begin, skip and ignore any data receieved before this time.
where <arg> is a boost::posix_time::ptime variable:

--start_epoch "2019-07-18 00:00:30"

Any start time specified in the configuration file will be overridden by using this command line argument.

### --end_epoch <arg>

Stop the processing at the specified time.
where <arg> is a boost::posix_time::ptime variable:

--end_epoch "2019-07-18 00:02:30"

Any end time specified in the configuration file will be overridden by using this command line argument.

### --max_epochs <arg>

Stop the processing after <arg> epochs have been processed.
where <arg> is an integer number of epochs:

--max_epochs 1

