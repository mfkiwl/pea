
# Ex01 - PPP Example

input_files:

    root_input_directory: /data/acs/pea/proc/iontest/products/

    antenna:        igs14_2045_plus.atx                         # required
    sinex:          AUS21027.SNX                              # required
    blqfile:        OLOAD_GO.BLQ                                # required if ocean loading is applied

    navigation:     brdm1150.20p                                # required but shouldn't!
    sp3file:        igs21025.sp3                            # cod21025.eph
    dcbfile:        CAS0MGXRAP_20201150000_01D_01D_DCB.BSX      # monthly DCB file
    ionfile:        codg1150.20i                                # IONEX file
    clkfile:        igs21025.clk_30s                                # Clk file

station_data:

    root_stations_directory: /data/acs/pea/proc/iontest/data/20115/

    files:
        # Select files to run by:
        #- <SEARCH_DIRECTORY>                           # - searching all in file_root directory
        - ALBY00AUS_S_20201150000_01D_30S_MO.rnx

output_files:

    root_output_directory:          /data/acs/pea/output/exs/<CONFIG>

    log_level:                      warn                             # debug, info, warn, error as defined in boost::log
    log_directory:                  ./
    log_filename:                   <CONFIG>-<YYYY><DDD><HH>.LOG

    output_trace:                   true
    trace_level:                    2
    trace_directory:                ./
    trace_filename:                 <CONFIG>-<STATION><YYYY><DDD><HH>.TRACE

    output_residuals:               false
    residuals_directory:            ./
    residuals_filename:             <CONFIG>-<YYYY><DDD><HH>.RES

    output_summary:                 false
    summary_directory:              ./
    summary_filename:               <CONFIG>-<YYYY><DDD><HH>.SUM

    output_ionex:                   false
    ionex_directory:                ./
    ionex_filename:                 IONEX.ionex
    iondsb_filename:                IONEX.iondcb

    output_plots:                   false
    plot_directory:                 ./
    plot_filename:                  plot.png


output_options:

    config_description:             EX01_SF_PPP
    analysis_agency:                GAA
    analysis_center:                Geoscience Australia
    analysis_program:               AUSACS
    rinex_comment:                  AUSNETWORK1


processing_options:

    start_epoch:                2020-04-24 00:00:00
    #end_epoch:                 2017-03-29 23:59:30
    #max_epochs:                 600        #0 is infinite
    epoch_interval:             30          #seconds

    process_modes:
        user:                   true
        network:                false
        minimum_constraints:    false
        rts:                    false
        ionosphere:             false

    process_sys:
        gps:            true
        glo:            false
        #gal:           true
        #bds:           true

    elevation_mask:     10   #degrees

    tide_solid:         true
    tide_pole:          true
    tide_otl:           true

    phase_windup:       true
    reject_eclipse:     true            #  reject observation during satellite eclipse periods
    raim:               true
    antexacs:           true

    cycle_slip:
        thres_slip: 0.05

    max_inno:   0
    max_gdop:   30

    troposphere:
        model:      gpt2   #vmf3
        #vmf3dir:    grid5/
        #orography:  orography_ell_5x5
        gpt2grid: gpt_25.grd

    pivot_station:        "USN7"
    pivot_satellite:      "G01"


ionosphere_filter_parameters:

    corr_mode:    total_electron_content   # broadcast, sbas, iono_free_linear_combo, estimate, total_electron_content
    model_noise:    0.001
    max_filter_iterations:  1
    max_filter_removals:    0


user_filter_parameters:

    max_filter_iterations:      2
    max_filter_removals:        2

    rts_lag:                    -1      #-ve for full reverse, +ve for limited epochs
    rts_directory:              ./
    rts_filename:               PPP-<CONFIG>-<STATION>.rts


network_filter_parameters:

    process_mode:           kalman      #lsq

    max_filter_iterations:      2
    max_filter_removals:        2


default_filter_parameters:

    stations:

        error_model:        elevation_dependent         #uniform elevation_dependent
        code_sigmas:        [0.15]
        phase_sigmas:       [0.0015]

        pos:
            estimated:          true
            sigma:              [0.1]
            proc_noise:         [1000]
            #apriori:                                   # taken from other source, rinex file etc.
            #frame:              xyz #ned
            #proc_noise_model:   Gaussian

        clk:
            estimated:          true
            sigma:              [0]
            proc_noise:         [1000]
            #proc_noise_model:   Gaussian

        clk_rate:
            estimated:          false
            sigma:              [0]
            proc_noise:         [1]

        amb:
            estimated:          true
            sigma:              [60]
            proc_noise:         [0]
            #proc_noise_dt:      day
            #proc_noise_model:   RandomWalk

        trop:
            estimated:          true
            sigma:              [0.1]
            proc_noise:         [0.0001]
            #proc_noise_dt:      min
            #proc_noise_model:   RandomWalk

        trop_grads:
            estimated:          true
            sigma:              [1]
            proc_noise:         [0.01]
            #proc_noise_dt:      hour
            #proc_noise_model:   RandomWalk

    satellites:

        clk:
            estimated:          false
            sigma:              [0]
            proc_noise:         [0.03651483716701108]
            #proc_noise_dt:      min
            #proc_noise_model:   RandomWalk

        clk_rate:
            estimated:          false
            sigma:              [0]
            proc_noise:         [1]

        orb:
            estimated:          false


custom:

    stations:
    #    ALIC:
    #        pos:
    #            sigma:              [60]
    #            proc_noise:         [0]
                ##proc_noise_model:   Gaussian
            #clk:
                #sigma:              [0.01]

        #AGGO:
            #exclude: true
        #ALIC:
            #exclude: true
        #ANKR:
            #exclude: true
                #network_estimated:  false

    satellites:

        ### Constellation Overrides
        #SYS_GPS:
            #srp:
                #sigma:              [0.01]
                #proc_noise:         [0.001]
                ##proc_noise_model:   RandomWalk

        #SYS_GAL:
            #clk:
                #sigma:              [1.0]
                #proc_noise:         [0.01]
                ##proc_noise_model:   RandomWalk

        #### Block Type Overrides
        #GPS-IIR-A:
            #pos: { sigma: [10.0] }
            #vel: { sigma: [0.1] }

        ### PRN Overrides
        #PRN_G10:
            #pos: { sigma: [10.0]    }
            #vel: { sigma: [0.1]     }
            #clk: { sigma: [1.0]     }

        #PRN_G15:
            #exclude: true

        ### SVN Overrides
        #SVN_G265:
            #pos: {sigma: [10.0] }
            #vel: {sigma: [0.1]  }
