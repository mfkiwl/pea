
# Ex00 - Test Example

input_files:

    root_input_directory: /data/acs/pea/proc/exs/products/

    atxfiles:   [igs14_2045_plus.atx                        ] # required
    snxfiles:   [igs19P2062.snx                             ] # required
    blqfiles:   [OLOAD_GO.BLQ                               ] # required if ocean loading is applied
    navfiles:   [brdm1990.19p                               ] # required but shouldn't!
    sp3files:   [gfm20624.sp3                               ]
    erpfiles:   [igs19P2062.erp                             ] #config parser doesn't accept weekly files yet.
    dcbfiles:   [CAS0MGXRAP_20191990000_01D_01D_DCB.BSX     ] # monthly DCB file
    clkfiles:   [jpl20624.clk                               ] # Clk file
    #podfiles:   [orb_partials/gag20624_orbits_partials.out ]   # only need this if we are estimating orbits


station_data:

    root_stations_directory: /data/acs/pea/proc/exs/data/

    files:
        # Select files to run by:

        - <SEARCH_DIRECTORY>                           # - searching all in file_root directory
        #- ALIC00AUS_R_20191990000_01D_30S_MO.rnx        # - selecting them individually below, or
                                                        # - selecting one on the command line using the -rnx option

output_files:

    root_output_directory:          /data/acs/pea/output/examples/<CONFIG>/

    log_level:                      error                             # debug, info, warn, error as defined in boost::log
    log_directory:                  ./
    log_filename:                   <CONFIG>-<YYYY><DDD><HH>.LOG

    output_trace:                   false
    trace_level:                    2
    trace_directory:                ./
    trace_filename:                 <CONFIG>-<STATION><YYYY><DDD><HH>.TRACE

    output_residuals:               false
    residuals_directory:            ./
    residuals_filename:             <CONFIG>-<YYYY><DDD><HH>.RES

    output_summary:                 false
    summary_directory:              ./
    summary_filename:               <CONFIG>-<YYYY><DDD><HH>.SUM

    output_ionex:                   false
    ionex_directory:                ./
    ionex_filename:                 IONEX.ionex
    iondsb_filename:                IONEX.iondcb

    output_plots:                   false
    plot_directory:                 ./
    plot_filename:                  plot.png

    output_clocks:                  false
    clocks_directory:               ./
    clocks_filename:                <CONFIG>.clk


output_options:

    config_description:             Ex00
    analysis_agency:                GAA
    analysis_center:                Geoscience Australia
    analysis_program:               AUSACS
    rinex_comment:                  AUSNETWORK1


processing_options:

    #start_epoch:               2019-07-18 23:39:00
    #end_epoch:                 2017-03-29 23:59:30
    max_epochs:                 1        #0 is infinite
    epoch_interval:             30          #seconds

    process_modes:
        user:                   true 
        network:                false 
        minimum_constraints:    false
        rts:                    false
        ionosphere:             false
        unit_tests:             true

    process_sys:
        gps:            true
        glo:            false
        #gal:           true
        #bds:           true

    elevation_mask:     10   #degrees

    tide_solid:         true
    tide_pole:          true
    tide_otl:           true

    phase_windup:       true
    reject_eclipse:     true            #  reject observation during satellite eclipse periods
    raim:               true
    antexacs:           true

    cycle_slip:
        thres_slip: 0.05

    max_inno:   0
    max_gdop:   30

    troposphere:
        model:      vmf3    #gpt2
        vmf3dir:    grid5/
        orography:  orography_ell_5x5
        # gpt2grid: EX03/general/gpt_25.grd

    pivot_station:        "USN7"
    pivot_satellite:      "G01"

unit_test_options:

    filename:       "/data/acs/pea/tests/Ex00Test"
    stop_on_fail:   true
    stop_on_done:   true
    output_errors:  true
    absorb_errors:  false

ionosphere_filter_parameters:

    model:        spherical_caps # bspline, spherical_harmonics
    lat_center:   -25
    lon_center:   135
    lat_width:    50
    lon_width:    80
    lat_resol:    20
    lon_resol:    32
    func_order:   4
    layer_heights: [450]
    model_noise:    0.001
    max_filter_iterations:  1
    max_filter_removals:    0


user_filter_parameters:

    max_filter_iterations:      2
    max_filter_removals:        2

    rts_lag:                    -1      #-ve for full reverse, +ve for limited epochs
    rts_directory:              ./
    rts_filename:               PPP-<CONFIG>-<STATION>.rts

network_filter_parameters:

    process_mode:           kalman      #lsq

    max_filter_iterations:      2
    max_filter_removals:        2


default_filter_parameters:

    stations:

        error_model:        elevation_dependent         #uniform elevation_dependent
        code_sigmas:        [0.15]
        phase_sigmas:       [0.0015]

        pos:
            estimated:          true
            sigma:              [0.1]
            proc_noise:         [0]
            #apriori:                               # taken from other source, rinex file etc.
            #frame:              xyz #ned
            #proc_noise_model:   Gaussian

        clk:
            estimated:          true
            sigma:              [0]
            proc_noise:         [1.8257418583505538]
            #proc_noise_model:   Gaussian

        clk_rate:
            estimated:          false
            sigma:              [0]
            proc_noise:         [1]

        amb:
            estimated:          true
            sigma:              [60]
            proc_noise:         [0]
            #proc_noise_dt:      day
            #proc_noise_model:   RandomWalk

        trop:
            estimated:          true
            sigma:              [0.1]
            proc_noise:         [0.3]
            proc_noise_dt:      hour
            #proc_noise_model:   RandomWalk

        trop_grads:
            estimated:          true
            sigma:              [1]
            proc_noise:         [0]
            #proc_noise_dt:      hour
            #proc_noise_model:   RandomWalk

    satellites:

        clk:
            estimated:          true
            sigma:              [0]
            proc_noise:         [0.03651483716701108]
            #proc_noise_dt:      min
            #proc_noise_model:   RandomWalk

        clk_rate:
            estimated:          false
            sigma:              [0]
            proc_noise:         [1]

        orb:
            estimated:          false


override_filter_parameters:

    stations:
        #ALICAUS01:
            #pos:
                #sigma:              [0.001]
                #proc_noise:         [0]
                ##proc_noise_model:   Gaussian
            #clk:
                #sigma:              [0.01]

        #AGGO:
            #exclude: true
        #ALIC:
            #exclude: true
        #ANKR:
            #exclude: true
                #network_estimated:  false

    satellites:

        ### Constellation Overrides
        #SYS_GPS:
            #srp:
                #sigma:              [0.01]
                #proc_noise:         [0.001]
                ##proc_noise_model:   RandomWalk

        #SYS_GAL:
            #clk:
                #sigma:              [1.0]
                #proc_noise:         [0.01]
                ##proc_noise_model:   RandomWalk

        #### Block Type Overrides
        #GPS-IIR-A:
            #pos: { sigma: [10.0] }
            #vel: { sigma: [0.1] }

        ### PRN Overrides
        #PRN_G10:
            #pos: { sigma: [10.0]    }
            #vel: { sigma: [0.1]     }
            #clk: { sigma: [1.0]     }

        #PRN_G15:
            #exclude: true

        ### SVN Overrides
        #SVN_G265:
            #pos: {sigma: [10.0] }
            #vel: {sigma: [0.1]  }


minimum_constraints:

    process_mode:       lsq     #kalman

    estimate:
        translation:    true
        rotation:       true
        #scale:          false   #not yet implemented

    station_default_noise: -1        #constrain none by default (negative numbers are not constrained)
    #station_default_noise: +1       #constrain all by default


    station_noise:
        ALIC: 0.001     #constrain strongly
        BAKR: 1
        ALIC: 100       #constrain weakly
